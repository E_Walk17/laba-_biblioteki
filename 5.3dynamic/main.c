#include <stdio.h>
#include <dlfcn.h>
int main(){
int rez1=0, trd_st=0, frt_st=0, rez2=0;
system("clear");
void *trd_s_lib;
void *frt_s_lib;
	int (*power_func)(int a);
	int (*power_func2)(int a);
	
	trd_s_lib = dlopen("/home/e_walk/docs/c_laby/5laba/5.3dynamic/libthird.so", RTLD_LAZY); 
			
			if (!trd_s_lib){ //если ошибка
			fprintf(stderr,"dlopen() error: %s\n", dlerror()); 
			return(1);
			};
	frt_s_lib = dlopen("/home/e_walk/docs/c_laby/5laba/5.3dynamic/libfourth.so", RTLD_LAZY); 
			
			if (!frt_s_lib){ //если ошибка
			fprintf(stderr,"dlopen() error: %s\n", dlerror()); 
			return(1);
			};


	printf("Библиотеки загружены!\n");
	power_func = dlsym(trd_s_lib, "trd_s");
	power_func2 = dlsym(frt_s_lib, "frt_s");
	printf("Введите число для возведения в 3-ю степень\n");
	scanf("%d", &trd_st);
	printf("Введите число для возведения в 4-ю степень\n");
	scanf("%d", &frt_st);
	
	rez1=(*power_func)(trd_st);
	rez2=(*power_func2)(frt_st);
	
	printf("%d^3 =  %d\n", trd_st, rez1);
	printf("%d^4 =  %d\n", frt_st, rez2);
	
	dlclose(trd_s_lib);
	dlclose(frt_s_lib);
	return 0;
	}
